def inverter(words):
    """
    Инвертирует последовательность слов, разделенных запятыми
    :param words: последовательность слов
    :return:
    """
    raw = words.split(',')
    raw.reverse()

    result = str(raw[0])
    for i in raw[1:]:
        result += "," + i

    return result


def tester():
    input_str = 'SIX,SEVEN,EIGHT,NINE,TEN'
    result = inverter(input_str)
    if result != 'TEN,NINE,EIGHT,SEVEN,SIX':
        print('Test #1 failed', 'input:' + input_str, 'result:' + result)
    else:
        print('Test #1 passed')

    input_str = 'SIX,SEVEN,EIGHT,NINE'
    result = inverter(input_str)
    if result != 'NINE,EIGHT,SEVEN,SIX':
        print('Test #2 failed', 'input:' + input_str, 'result:' + result)
    else:
        print('Test #2 passed')


if __name__ == '__main__':
    tester()

def splitter(sentence):
    """
    Функция разбивающая предложения на слова и на основе этих слов формирует
    сложенный список содержащий все символы этих слов
    :param sentence: предложение
    :return:
    """
    raw_words = [word.strip(".,:;") for word in sentence.split()]
    result = []
    for i in raw_words:
        result.append(list(i))

    return result


def tester():
    input_str = 'Eeny, meeny, miney, moe; Catch a tiger by his toe.'
    correct_result = [['E', 'e', 'n', 'y'], ['m', 'e', 'e', 'n', 'y'], ['m', 'i', 'n', 'e', 'y'],
                      ['m', 'o', 'e'], ['C', 'a', 't', 'c', 'h'], ['a'], ['t', 'i', 'g', 'e', 'r'],
                      ['b', 'y'], ['h', 'i', 's'], ['t', 'o', 'e']]

    result = splitter(input_str)

    if result != correct_result:
        print('Test #1 failed', 'input:' + input_str, 'result:' + str(result))
    else:
        print('Test #1 passed')


if __name__ == '__main__':
    tester()
